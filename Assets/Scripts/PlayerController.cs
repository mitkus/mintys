using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	// Tweaks
	public float moveSpeed = 10.0f;
	public float shootRate = 10.0f;
	public Camera gameCamera;
	public GameObject bullet;
	public GameObject bulletSpawnEffect;
	
	public AudioClip shootSound;
	
	// State
	float lastBulletT = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	void FixedUpdate() {
		float dx = -Input.GetAxis("Horizontal");
		float dy = Input.GetAxis("Vertical");
		
		// Move/rotate player
		if(dx != 0.0f || dy != 0.0f) {
			Vector2 d = new Vector2(dx, dy);
			d.Normalize();
			d *= moveSpeed;
			
			float dir = Mathf.Atan2(d.y, d.x) + Mathf.PI;
			Quaternion targetRot = Quaternion.Euler(new Vector3(0.0f, 0.0f, dir / Mathf.PI * 180.0f));
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, 0.3f);
			
			rigidbody.AddForce(new Vector3(dx, dy, 0.0f));
		}
		
		// Spawn bullets
		float bx = -Input.GetAxis("ShootX");
		float by = -Input.GetAxis("ShootY");
		
		if(Input.GetMouseButton(0)) {
			Vector3 screenMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);			
			Vector3 mouseDir = gameCamera.ScreenToWorldPoint(screenMouse) - transform.position;
			bx = mouseDir.x;
			by = mouseDir.y;
		}
		
		if(bx != 0.0f || by != 0.0f) {
			Vector3 d = new Vector3(bx, by, 0.0f);
			d.Normalize();
			d *= 1.6f;
			
			float t = Time.fixedTime;
			if(t - lastBulletT > 1.0f / shootRate) {
				float dir = Mathf.Atan2(d.y, d.x);
				Vector3 bulletPos = transform.position + d;
				Quaternion bulletRot = Quaternion.Euler(new Vector3(0.0f, 0.0f, dir / Mathf.PI * 180.0f));
				GameObject b = (GameObject)Instantiate(bullet, bulletPos, bulletRot);
				b.rigidbody.velocity = rigidbody.velocity;
				
				Quaternion effectRot = Quaternion.LookRotation(d);
				GameObject e = (GameObject)Instantiate(bulletSpawnEffect, transform.position + d * 0.5f, effectRot);
				e.transform.parent = gameObject.transform;
				Destroy(e, 1.0f);
				gameObject.audio.PlayOneShot(shootSound);
				lastBulletT = t;
			}
		}
			
		// Move camera
		Vector3 pos = transform.position;
		Vector3 cameraPos = gameCamera.transform.position;
		cameraPos = Vector3.Lerp(cameraPos, new Vector3(pos.x, pos.y, cameraPos.z), 0.1f);
		gameCamera.transform.position = cameraPos;
	}
}
