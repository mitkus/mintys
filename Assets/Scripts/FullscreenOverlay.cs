using UnityEngine;
using System.Collections;

public class FullscreenOverlay : MonoBehaviour {
	void Update () {
		guiTexture.pixelInset = new Rect(0.0f, 0.0f, Screen.width, Screen.height);
	}
}
