using UnityEngine;
using System.Collections;

public class CheckGameover : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if(transform.childCount == 0) {
			GameObject HUD = GameObject.Find("HUD");
			HUD.BroadcastMessage("ShowGameover");
		}
	}
}
