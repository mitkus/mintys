using UnityEngine;
using System.Collections;

public class SpawnFood : MonoBehaviour {
	
	public Vector3 boundsMin = new Vector3(-500.0f, -500.0f, 0.0f);
	public Vector3 boundsMax = new Vector3(500.0f, 500.0f, 0.0f);
	public int amount = 10000;
	public GameObject food;

	void Start () {
		for(uint i = 0; i < amount; ++i) {
			float x = Random.Range(boundsMin.x, boundsMax.x);
			float y = Random.Range(boundsMin.y, boundsMax.y);
			float z = 0.0f;
			
			Quaternion rot = Quaternion.Euler(new Vector3(0.0f, 0.0f, Random.Range(0, 360.0f)));
			
			GameObject obj = (GameObject)Instantiate(food, new Vector3(x, y, z), rot);
			obj.transform.parent = gameObject.transform;
		}
	}
}
