using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	
	// 0, 1 or 2
	// 0 adds instability until idea splits
	// 1 adds weight
	// 2 adds size
	private int type;
	
	public Color colorInstability;
	public Color colorWeight;
	public Color colorSize;
	
	private Color[] colors;
		
	// Use this for initialization
	void Start () {
		colors = new Color[3];
		colors[0] = colorInstability;
		colors[1] = colorWeight;
		colors[2] = colorSize;
		
		type = Random.Range(0, 3);
		renderer.material.color = colors[type];
	}
	
	void OnCollisionEnter(Collision collision) {
		if(collision.gameObject.name.StartsWith("Idea")) {
			Destroy(gameObject);
			collision.gameObject.BroadcastMessage("Grow", type);
		}
	}
}
