using UnityEngine;
using System.Collections;

public class KeepDistanceToPlayer : MonoBehaviour {
	
	public float maxDistanceToPlayer;
	public float minDistanceToPlayer;
	
	GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 d = player.transform.position - transform.position;
		float len = d.magnitude;
		d.Normalize();
	
		// Move towards player
		if(len > maxDistanceToPlayer) {
			float f = (len - maxDistanceToPlayer) / 10.0f;
			d *= f;	
			rigidbody.AddForce(d);
		}
		
		// Move away from player
		if(len < minDistanceToPlayer) {
			float f = minDistanceToPlayer - len;
			d *= f / 10.0f;
			rigidbody.AddForce(-d);
		}	
	}
}
