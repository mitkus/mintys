using UnityEngine;
using System.Collections;

public class SpawnEnemies : MonoBehaviour {
	
	public GameObject enemy;
	public float spawnDistance = 16.0f;
	public float spawnInterval = 5.0f;
	
	GameObject player;
	float lastSpawnT;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player");
		lastSpawnT = Time.timeSinceLevelLoad;
		Enemy.lastDeathT = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.timeSinceLevelLoad;
		int maxAliveEnemies = Mathf.FloorToInt(t / 45.0f);
		int aliveEnemies = transform.GetChildCount();
		if(aliveEnemies < maxAliveEnemies && t - lastSpawnT > spawnInterval && t - Enemy.lastDeathT > spawnInterval) {
			Vector3 off = Random.onUnitSphere;
			off.z = 0.0f;
			off.Normalize();
			Instantiate(enemy, player.transform.position + off * spawnDistance, Quaternion.identity);
			lastSpawnT = t;
		}
	}
}
