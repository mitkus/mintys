using UnityEngine;
using System.Collections;

public class Idea : MonoBehaviour {
	private float size = 1.0f;
	private float weight = 1.0f;
	private float jitter = 1.0f;
	
	public Color colorInstability;
	public Color colorWeight;
	public Color colorSize;
	
	public GameObject dieEffect;
	public GameObject eatEffect;
	
	public AudioClip soundA;
	public AudioClip soundB;
	public AudioClip soundC;
	public AudioClip explodeSound;
	
	public static int score = 0;

	void Start () {
		GameObject parent = GameObject.Find("Ideas");
		transform.parent = parent.transform;
		Refresh();
	}
	
	void Grow(int type) {
		GameObject o = (GameObject)Instantiate(eatEffect);
		Vector3 pos = transform.position;
		pos.z = 10.0f;
		o.transform.position = pos;
		Destroy(o, 1.0f);
		
		score++;
		if(type == 0) {
			jitter += 1.0f;
			gameObject.audio.PlayOneShot(soundA);
		}
		if(type == 1) {
			weight += 1.0f;
			gameObject.audio.PlayOneShot(soundB);
		}
		if(type == 2) {
			size += 1.0f;
			gameObject.audio.PlayOneShot(soundC);
		}
		
		if(jitter > 6.0f) {
			// Split into two ideas
			Vector2 off = Random.insideUnitCircle * 2.0f;
			Vector3 off3 = new Vector3(off.x, off.y, 0.0f);
			Vector3 pos1 = transform.position + off3;
			Vector3 pos2 = transform.position + off3;
			size = Mathf.Max(1.0f, Random.Range(0.0f, 2.0f));
			weight = Mathf.Max(1.0f, Random.Range(0.0f, 2.0f));
			jitter = Mathf.Max(1.0f, Random.Range(0.0f, 2.0f));
			Instantiate(gameObject, pos1, Quaternion.identity);
			size = Mathf.Max(1.0f, Random.Range(0.0f, 2.0f));
			weight = Mathf.Max(1.0f, Random.Range(0.0f, 2.0f));
			jitter = Mathf.Max(1.0f, Random.Range(0.0f, 2.0f));
			Instantiate(gameObject, pos2, Quaternion.identity);
			Destroy(gameObject);
			
			Camera.main.audio.PlayOneShot(soundA);
			Camera.main.audio.PlayOneShot(soundB);
			Camera.main.audio.PlayOneShot(soundC);
		}
		else {
			Refresh();
		}
	}
	
	void Shrink() {
		size = Mathf.Max(0.0f, size - 0.1f);
		weight = Mathf.Max(0.0f, weight - 0.1f);
		jitter = Mathf.Max(0.0f, jitter - 0.1f);
		
		if(size + weight + jitter <= 0.0f) {
			// Die
			Destroy(gameObject);
			Object o = Instantiate(dieEffect, transform.position, Quaternion.identity);
			Camera.main.audio.PlayOneShot(explodeSound);
			Destroy(o, 1.0f);	
		}
		else {
			Refresh();
		}
	}
	
	void Refresh() {
		float life = size + weight + jitter;
		if(life > 0.0f) {
			Color c = size / life * colorSize;
			c += weight / life * colorWeight;
			c += jitter / life * colorInstability;
			renderer.material.color = c;
		}
		else {
			renderer.material.color = Color.gray;
		}
		
		rigidbody.mass = 0.01f + weight * 0.01f;
		float s = 1.0f + size / 5.0f;
		transform.localScale = new Vector3(s, s, s);
	}
	
	void FixedUpdate () {
		if(jitter > 1.0f) {
			Vector3 jitterOffset = Random.insideUnitSphere * (jitter-1.0f) / 3.0f;
			rigidbody.AddForce(jitterOffset);
		}
		
		// Look for food nearby, go towards it
		Collider[] hits = Physics.OverlapSphere(transform.position, transform.localScale.x * 2.0f);
		foreach(Collider hit in hits) {
			GameObject obj = hit.gameObject;
			if(obj.name.StartsWith("Food")) {
				Vector3 d = (obj.transform.position - transform.position).normalized;
				rigidbody.AddForce(d * 2.0f);
				break;
			}
		}
	}
}
