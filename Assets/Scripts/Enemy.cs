using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	
	public float baseHealth = 20.0f;
	public float shootRate = 10.0f;
	public GameObject bullet;
	public GameObject dieParticles;
	public GameObject bulletSpawnEffect;
	
	public AudioClip explodeSound;
	public AudioClip shootSound;
	
	private float lastBulletT = 0.0f;
	private float health;
	private float maxHealth;
	public static float lastDeathT = 0.0f;
	
	GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player");
		GameObject parent = GameObject.Find("Enemies");
		transform.parent = parent.transform;
		health = baseHealth * Mathf.Max(1.0f, (Time.timeSinceLevelLoad / 60.0f));
		maxHealth = health;
	}
	
	void Hit() {
		health -= 3.0f;
		if(health <= 0.0f) {
			lastDeathT = Time.timeSinceLevelLoad;
			Camera.main.audio.PlayOneShot(explodeSound);
			Destroy(gameObject);
			Object o = Instantiate(dieParticles, transform.position, Quaternion.identity);
			Destroy(o, 1.0f);
		}
		else {
			float s = 0.7f + (health / maxHealth) * 0.8f; 
			transform.localScale = new Vector3(s, s, s);
		}
	}
	
	void Shoot(float t) {
		// Select closest idea in 10 unit radius
		GameObject target = null;
		Collider[] hits = Physics.OverlapSphere(transform.position, 10.0f);
		foreach(Collider hit in hits) {
			GameObject obj = hit.gameObject;
			if(obj.name.StartsWith("Idea")) {
				if(target == null) {
					target = obj;
				}
				else {
					float distance = Vector3.Distance(target.transform.position, transform.position);
					float hitDistance = Vector3.Distance(obj.transform.position, transform.position);
					bool isCloser = hitDistance < distance;
					if(isCloser)
						target = obj;
				}
			}
		}
		
		// Shoot
		if(target) {
			Vector3 d = (target.transform.position - transform.position).normalized;
			d *= 1.6f;
			float dir = Mathf.Atan2(d.y, d.x);
			Vector3 bulletPos = transform.position + d;
			Quaternion bulletRot = Quaternion.Euler(new Vector3(0.0f, 0.0f, dir / Mathf.PI * 180.0f));
			GameObject b = Instantiate(bullet, bulletPos, bulletRot) as GameObject;
			b.rigidbody.velocity = rigidbody.velocity;
			
			Quaternion effectRot = Quaternion.LookRotation(d);
			GameObject e = (GameObject)Instantiate(bulletSpawnEffect, transform.position + d * 0.5f, effectRot);
			e.transform.parent = gameObject.transform;
			Destroy(e, 1.0f);
			
			gameObject.audio.PlayOneShot(shootSound);
			
			lastBulletT = t;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// Randomly rotate sometimes
		if(Random.Range(0, 50) == 42) {
			rigidbody.AddTorque(0.0f, 0.0f, Random.Range(-0.5f, 0.5f));
		}
		
		// Move tangentialy around player
		if(Random.Range(0, 100) == 10) {
			Vector3 d = (player.transform.position - transform.position).normalized;
			Vector3 up = new Vector3(0.0f, 0.0f, 1.0f);
			Vector3 dir = Vector3.Cross(d, up);
			float f = Random.Range(-10.0f, 10.0f);
			rigidbody.AddForce(dir * f);
		}
		
		// Shoot ideas
		float t = Time.fixedTime;
		if(t - lastBulletT > 1.0f / shootRate) {
			Shoot(t);
		}
	}
}
