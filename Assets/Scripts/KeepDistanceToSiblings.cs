using UnityEngine;
using System.Collections;

public class KeepDistanceToSiblings : MonoBehaviour {
	
	public float minDistanceToSiblings;

	void FixedUpdate () {
		// Move away from other ideas
		Transform parent = gameObject.transform.parent;
		for(int i = 0; i < parent.childCount; ++i) {
			Transform t = parent.GetChild(i);
			if(t != transform) {
				Vector3 td = t.position - transform.position;
				float len = td.magnitude;
				if(len < minDistanceToSiblings) {
					float tf = minDistanceToSiblings - len;
					td *= tf / 10.0f;
					rigidbody.AddForce(-td);
				}
			}
		}
	}
}
