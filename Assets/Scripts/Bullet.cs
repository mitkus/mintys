using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	
	public float speed;
	
	public GameObject hitParticles;
	
	public AudioClip hitSound;

	// Use this for initialization
	void Start () {
		float scatter = Random.Range(-speed/20.0f, speed/20.0f);
		rigidbody.AddRelativeForce(new Vector3(speed, scatter, 0.0f));
		rigidbody.transform.parent = GameObject.Find("Bullets").transform;
		
		Destroy(gameObject, 4.0f);
	}
		
	void OnCollisionEnter(Collision collision) {
		if(collision.gameObject.name.StartsWith("Idea")) {	
			collision.gameObject.BroadcastMessage("Shrink");
		}
		if(collision.gameObject.name.StartsWith("Enemy")) {
			collision.gameObject.BroadcastMessage("Hit");
		}
		
		Quaternion rot = Quaternion.LookRotation(collision.contacts[0].normal);
		Object particles = Instantiate(hitParticles, transform.position, rot);
		Destroy(particles, 1.0f);
		
		Destroy(gameObject);
		
		Camera.main.audio.PlayOneShot(hitSound);
	}
}
