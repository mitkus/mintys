using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {
	
	GameObject text;
	GameObject title;
	GameObject overlay;
	
	bool gameOver = false;
	bool intro = false;
	
	float introStart = 0.0f;
	
	// Use this for initialization
	void Start () {
		text = GameObject.Find("HUD/Text");
		title = GameObject.Find("HUD/Title");
		overlay = GameObject.Find("HUD/Overlay");
		
		title.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		overlay.guiTexture.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
		
		if(Time.realtimeSinceStartup < 10.0f)
			ShowIntro();
	}
	
	void ShowIntro() {
		intro = true;
		introStart = Time.time;
	}
	
	void ShowGameover() {
		intro = false;
		overlay.guiTexture.color = new Color(0.0f, 0.0f, 0.0f, 0.3f);
		text.guiText.text = "your idea is dead,\nyou fed "+Idea.score.ToString()+" thoughts to it\n\n[r] to restart, [q] to quit";
		text.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		gameOver = true;
	}
	
	float SinFade(float start, float end, float t) {
		float l = end - start;
		return Mathf.Sin((t - start) / l * Mathf.PI);
	}
	
	// Update is called once per frame
	void Update () {
		if(gameOver && Input.GetKeyDown(KeyCode.R)) {
			Application.LoadLevel(Application.loadedLevel);
			Idea.score = 0;
		}
		
		if(Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
		
		if(intro) {
			float t = Time.time - introStart;
			if(t <= 4.0f) {
				float alpha = SinFade(1.0f, 3.5f, t);
				text.guiText.text = "a fragile idea of yours";
				text.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, alpha); 
			}
			else if(t <= 7.0f) {
				float alpha = SinFade(4.0f, 6.5f, t);
				text.guiText.text = "protect it";
				text.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, alpha); 
			}
			else if(t <= 10.0f) {
				float alpha = SinFade(7.0f, 9.5f, t);
				text.guiText.text = "feed it";
				text.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, alpha); 
			}
			else if(t <= 13.0f) {
				float alpha = SinFade(10.0f, 12.5f, t);
				text.guiText.text = "evolve it into something big";
				text.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, alpha); 
			}
			else if(t <= 21.0f) {
				float alpha = SinFade(15.0f, 20.0f, t);
				title.guiText.material.color = new Color(1.0f, 1.0f, 1.0f, alpha); 
			}
			if(t > 21.0f){
				text.guiText.text = "";
				intro = false;
			}
		}
	}
}